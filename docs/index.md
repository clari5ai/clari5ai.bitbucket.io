# Clari5 AI

## Java Modules List

 - [Manager](./containers/manager)
 - [Job](./containers/job)
 - [FACT](./containers/fact)
 - [UAC](./containers/uac)
 - [CMS](./containers/cms)

## Frontend Modules List

 - [App](./frontend/app)
 - [Fact](./frontend/fact)
 - [Query Builder](./frontend/queryBuilder)
 - [UAC](./frontend/uac)

## Internal Library Modules List

 - [Common](./libs/common)
 - [DI](./libs/di)
 - [MS-core](./libs/ms-core)



