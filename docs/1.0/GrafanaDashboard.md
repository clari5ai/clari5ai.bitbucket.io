
# Grafana Dashboard APIs

### Create dashboard through APIs

 - Create an Organization in existing admin

```http
POST /api/orgs HTTP/1.1
Accept: application/json
Content-Type: application/json

{
  "name": "New Organization."
}
```


 - Set created Organization to current Organization

```http
POST http://admin:cxps123@localhost:3000/api/user/using/:organizationId HTTP/1.1
Accept: application/json
Content-Type: application/json
```


* Create a datasource in existing Organization \

```http
POST http://admin:cxps123@localhost:3000/api/datasources HTTP/1.1
Accept: application/json
Content-Type: application/json

{
  "name": "Dashboard Name",
  "type": "elasticsearch",
  "url": "http://localhost:9200",
  "access": "proxy",
  "basicAuth": false,
  "orgId": 3,
  "isDefault": true,
  "database": "[telegraf-]YYYY.MM.DD",
  "jsonData": {
    "interval": "Daily",
    "esVersion": 60
  }
}
```



* Create a user

```http
POST http://admin:cxps123@localhost:3000/api/admin/users HTTP/1.1
Accept: application/json
Content-Type: application/json

{
  "name": "User",
  "email": "user@graf.com",
  "login": "user",
  "password": "userpassword"
}
```



* Create/Update Dashboard

```http
POST /api/dashboards/db HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer eyJrIjoiT0tTcG1pUlY2RnVKZTFVaDFsNFZXdE9ZWmNrMkZYbk

{
  "dashboard": {
    "id": null,
    "uid": null,
    "title": "Production Overview",
    "tags": [
      "templated"
    ],
    "timezone": "browser",
    "schemaVersion": 16,
    "version": 0
  },
  "folderId": 0,
  "overwrite": false
}
```
