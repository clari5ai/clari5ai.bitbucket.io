
# Build Steps

!!! Important
    Make sure **CLARI5_HOME** is exported to root directory of this git repo for all below commands to work.

### Building and Packaging all modules

This will build all the modules from start and package them.

```bash
${CLARI5_HOME}/bin/scripts/docker-build.sh "<external_version>" "<internal_version>" && \
${CLARI5_HOME}/bin/scripts/package-int.sh "<external_version>" "<internal_version>" && \
${CLARI5_HOME}/bin/scripts/package-ext.sh "<external_version>" "<internal_version>" && \
${CLARI5_HOME}/bin/scripts/package-install.sh "<external_version>" "<internal_version>"
```

### Building all modules
This will build all the modules.

```bash
${CLARI5_HOME}/bin/scripts/docker-build.sh "<external_version>" "<internal_version>" 
```

### Packaging
Packaging includes three files : 
 - clari5ai_install.tar.gz
 - images-ext-<ext_ver>.tar
 - images-int-<int_ver>.tar

`clari5ai_install.tar.gz` - 
    Has all the scripts and related files required to deploy applications using images.
    It also contains images related to setup of deployment.

`images-ext-<ext_ver>.tar` - 
    Has all the external images that need to be deployed. 
    There is a different package for external and different for internal images, as internal images are expected to be released more frequently than external packages.

`images-int-<int_ver>.tar` - 
    Has all the internal images and is considerably smaller in size than `images-ext-<ext_ver>.tar`, 
    and will be more frequently updated.


#### Internal Images Packaging
Run following command to create the package for internal images.
```bash
${CLARI5_HOME}/bin/scripts/package-int.sh "<internal_version>"
```

#### External Images Packaging
Run following command to create the package for external images.
```bash
${CLARI5_HOME}/bin/scripts/package-ext.sh "<external_version>"
```

#### Installation Packaging
Run following command to create the package for docker files and other extra files.
```bash
${CLARI5_HOME}/bin/scripts/package-install.sh "<external_version>" "<internal_version>"
```


# Creating a Patch

 - In Clari5 Genie App, patch can be created using only data_center image.
 - Build all components as specified [here](#Building-all-modules). Make sure all components are build as patch cannot be sent partially for one module and all module changes will be sent.
 - Run the following
 - Build the data_center image using following command.
```bash
cd ./bin/docker/apps
docker-compose -f clari5-docker-build.yml build clari5.data_center
```

### Deploying changes on on AWS
 - If you need to push changes to Clari5 AWS, you need to push image to a registry which is accessible over the internet. (AWS has its own registry but is not accessible for push)
 - Run following command to send it to GenieDemo
```bash
docker image tag clari5/data_center:snapshot geniedemo.clari5.com:9898/clari5/data_center:snapshot
docker image push geniedemo.clari5.com:9898/clari5/data_center:1.2
```
 - Now that image is accessible on GenieDemo
```bash
docker image pull geniedemo.clari5.com:9898/clari5/data_center:1.2
docker image tag geniedemo.clari5.com:9898/clari5/data_center:snapshot clari5/data_center:1.2
```
 - Now to deploy the changes
```bash
docker service update --force runtime-stack_data_center
```
 - UI changes are now available, if there are any other changes in other modules, similarly restart the services as required, eg. manager
```bash
docker service update --force runtime-stack_manager
```

# Publishing package

Copy the created installer into
```bash
scp clari5ai_install.tar.gz root@registry.clari5.com:/root/installers/1.2/
```


