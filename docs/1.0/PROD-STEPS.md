# Genie Installation

## Installing Docker on clean machine

!!! info
    Steps tested on Ubuntu 18.04 and CentOS 7

## Steps to install (Ubuntu):

```bash
sudo apt-get update

sudo apt-get install -y \
           apt-transport-https \
           ca-certificates \
           curl \
           software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
   
sudo apt-get update

sudo apt-get install -y docker-ce
```

## Steps to install (CentOS):

```bash
sudo yum install -y \
            yum-utils \
            device-mapper-persistent-data \
            lvm2

# Make sure that System time is correct
# Follow http://sysops.km2gd.com/adding-docker-ce-repo-to-centos-peers-certificate-has-expired-yum/
sudo yum-config-manager \
            --add-repo \
            https://download.docker.com/linux/centos/docker-ce.repo

sudo yum install -y docker-ce

sudo systemctl start docker
```
??? help "Need help with an Error?"
    If you get an error 
    `Error: Package: 3:docker-ce-18.09.6-3.el7.x86_64 (docker-ce-stable)
                        Requires: container-selinux >= 2.9 `
    Go to http://mirror.centos.org/centos/7/extras/x86_64/Packages/, and find the installer that suits your system for the package `container-selinux`. 
    Download and install it, and then run docker-ce installation again.

## How to test 
Wait for a while as this requires internet connection to pull the hello-world image.

```bash
sudo docker run hello-world
```

## Removing sudo from docker

```bash
sudo groupadd docker
sudo gpasswd -a $USER docker

# Logout all users or run the following command
newgrp docker

# Now you should be able to run following without sudo
docker run hello-world
```

## Install docker-compose

```bash
docker-compose --version # Check if it is already installed
sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
```

## Configure docker daemon

!!! Important
    `master_node_ip` below is the IP of machine which is reachable by all other machines working in this cluster.
    Master node IP refers to machine where installation is being done, as only this machine will have an instance of registry.
    Set this to 127.0.0.1 if deployment is single machine.

 - Add the following in `/etc/docker/daemon.json` and restart docker daemon. (Requires sudo permission)
```json
{
    "log-driver": "json-file",
    "log-opts": {
        "max-size": "100m",
        "max-file": "10"
    },
    "registry-mirrors" : [ "https://registry.clari5.com:19443", "http://<master_node_ip>:9898" ],
    "insecure-registries" : [ "<master_node_ip>:9898" ]
}
```
 - Restart docker using
```bash
sudo systemctl restart docker
```

## Install MYSQL / MariaDB *(Optional)*

!!! Note
    Genie Stack already contains MariaDB configured for to work with Genie, following steps are only required 
    if you need MYSQL not to be running inside docker.

Installation manual for MariaDB on [CentOS 7][centos mariadb install] or on [Ubuntu][ubuntu mysql install]

In any of the installation, make sure that one user (which be used by Genie Applications) has accessible on '%' host, as docker will be accessing DB via different IPs.
If not enabled, you can enable this by following query (replace DB_USER and DB_PASS with database username and password)
```mysql
GRANT ALL ON *.* TO '<$DB_USER>'@'%' IDENTIFIED BY '<$DB_PASS>'
```

[centos mariadb install]: https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-centos-7
[ubuntu mysql install]: https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-debian-9

Make sure to update `runtime/conf/prod.env` with new Database credentials.

!!! Important
    If **DB_HOST** is `localhost` or `127.0.0.1` for your setup, 
    use the output IP of `#!bash ip addr show docker0 | grep "inet "`
    this would usually be `172.17.0.1`

## To setup machine for deployment

#### One-time setup
 - Run the following to create clari5_home directory
```bash
mkdir clari5_home
cd clari5_home
export CLARI5_HOME=`pwd`
echo "export CLARI5_HOME=${CLARI5_HOME}" >> ~/.bashrc
```
 - Copy the `clari5ai_install.tar.gz` and all installers into CLARI5_HOME.
```bash
cp <path_to_installers> ${CLARI5_HOME}
tar xvzf clari5ai_install.tar.gz
./setup.sh "<ext_ver>"
```
 - At this point you need to make sure that Database is ready and the user has permission that has access to MYSQL from '%' host. Also MYSQL should be reachable from all the machines where Genie is being deployed.
 - Edit `runtime/conf/prod.env` file to update DB_HOST, DB_PORT, DB_USER, DB_PASS according to environment
!!! warning
    ***Do not use localhost in any environment variable***
 - If DB_HOST is `localhost`, use the output IP of `#!bash ip addr show docker0 | grep "inet "`, this would usually be `172.17.0.1`

#### Setting up worker nodes (Only for multi-machine/cluster setup)
 - On each worker node in cluster, make sure `docker` is installed, 
   and all machines in cluster are reachable to each other.
 - Run the following on master node
```bash
docker swarm join-token worker
```
 - This will output a command that needs to run on each machine which will work as worker node in a cluster setup.

#### Bring up stack
!!! Note
    Stack only needs to be brought up on master node and deployment will be done on all worker nodes.

 - Make sure all the images tar files are inside CLARI5_HOME.
 - On master node, run following command to deploy the stack.
```bash
cp "<installer_files>" ${CLARI5_HOME}
./deploy.sh "<ext_ver>" "<int_ver>"
```
 - By default this will bring up self signed server on BASE_DN specified in `runtime/conf/prod.env`. To setup with signed certificate, go to last section. 

# Using Signed Certificates

 - To use signed certificates instead of self-signed certificates, copy the certificate with name `runtime/conf/certs/clari5.crt` and the key with name `runtime/conf/certs/clari5.key`.
 - Open `runtime/conf/clari5web.dockerFile` and uncomment following lines in it, like this.
```dockerfile
COPY certs/clari5.key /certs/clari5.key
COPY certs/clari5.bundle.crt /certs/clari5.crt
COPY nginx.template.cert.conf /tmp/nginx.template.conf
```
 - And comment following line in it, like this
```dockerfile
# COPY nginx.template.self.conf /tmp/nginx.template.conf
```
 - If the deployment is already done, run the following, to reflect the changes. Else skip this step and continue where you left.
```bash 
./deploy.sh "<ext_ver>" "<int_ver>"
docker service update --force runtime-stack_clari5web
```

