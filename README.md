
# Clari5 Docs

This repository contains all the documents that need to be shared publicly for building, installation and deployment of Clari5AI projects.


# How to update

To generate the static content, 
 - Clone this repository
 - Install [Mkdocs](https://www.mkdocs.org/) using pip and run mkdocs build on root directory as follows
```bash
pip install mkdocs mkdocs-material
mkdocs build --clean
```
 - Commit and push the changes in site directory as is in master branch.

 > **Note**: Make sure that files that are not related to documentation but need to be in this 
         repo are committed in assets directory. This will make sure they are not replicated in site directory.