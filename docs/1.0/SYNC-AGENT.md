# Sync Agent

 - [Building](#building)
 - [Deploying](#deploying)
 - [AWS Changes](#AWS-Changes)

## Building

To Build package for Sync Agent, run the following.
!!! Note
    Make sure ${CLARI5_HOME} is set.
```bash
./bin/scripts/create-mailer-stack.sh
```
This will create a tar file named 'mailer-stack.tar.gz' in ${CLARI5_HOME}.

```
./bin/scripts/create-catcher.sh
```
This will create a tar file named 'mailer-stack-catcher.tar.gz'. This tar is to be copied into every EFM instance, where stats need to be collected.


# How it works

There is a Monitoring System deployed on Cloud and there is a data sync mechanism that is put in place at Bank Site to extract certain data points from EFM application and push to the cloud. 

On cloud further processing of these data point happens and then it is stored in secured environment to be used for representation on various graphs.

A brief explanation of this process is as follows - Sync agent is installed as a new component in bank’s env Sniffing agent is installed on all the machines running Clari5 application Sniffing agent sends data to Sync Agent Sync agent compresses the data and send the data out as an attachment in an Email (Via Banks SMTP to ensure auditable route) every 5 minutes.

There is an email listener kept at Clari5 cloud which extracts this attachment and processes this data.

This data is collected at Clari5 Central Dashboarding deployment


# Requirements

#### Hardware Requirements

*   Sync Agent Machine (4 Core , 8 GB Ram , 500 GB HDD) 
*   Add the sync agent machine within the same network as Clari5 
*   Port to be open on sync agent (9000, 9087, 8186) 
*   Access to Bank SMTP server for sending mail with 5 MB attachments 
*   Email Server, Port, User and Password details. (optional) Auto archive sent mails every 3 hours
*   One time root access on all Clari5 machines (to install telegraf) 

#### Software Requirement 

*   Telegraf (on EFM instances)
*   Docker (on Collector instance)


## Deploying

### Prerequisite

Sync Agent requires docker to be installed on one of the machines and also requires it to be in the same network. In this document we will call this instance as MAILER. 

Follow steps on [Docker Docs](https://docs.docker.com/install/) to install docker on mailer machine.

Also mailer should also be accessible via browser, as we need access NIFI on it.

Sync Agent also requires an telegraf to be installed on each of the EFM instances. 

Follow steps on [Telegraf Docs](https://docs.influxdata.com/telegraf/v1.11/introduction/installation/) to install telegraf on EFM instances. If instance is RHEL or Ubuntu flavours of Linux, their respective rpm and deb files are added in package by default.

### Deploying mailer stack

 - Untar mailer-stack.tar.gz
 - Open conf/prod.env and change the BANK_ID to corresponding <tenant_id>.
 - Run the following command

```bash
./deploy-mailer-stack.sh
```
 - After deployment is done.
 - Open Nifi using http://localhost:9087/nifi
 - Upload `SendMailAnyTenant.xml` template using Right Click -> Upload template
 - Drag template box from top of the screen to dashboard
 - Right click the SendMail and click _Configure_. (Added screenshots below for reference) **Update the following properties:**
    - SMTP _hostname_
    - SMTP _Port_
    - _Username_
    - _Password_ 
    - _From_ Email Address (same as Username)
    - _To_ Email address should be of format: **_TENANT@sync.clari5now.com_**
 - Click the Start button on the left panel (Added screenshots below for reference). Follow the Testing to make sure entered credentials work.

## Testing
 - Log into the machine where mailer-stack is deployed.
 - To verify if all containers are up and running open Portainer in Browser ( http://localhost:9000 ).
   Run the following to bring up portainer if it not running.
```bash
docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
```
 - Send following curl request to send a sample data to mailer-stack.
```bash
curl -i -XPOST 'http://localhost:8186/write?db=mydb' --data-binary 'cpu_load_short,host=server01,region=us-west value=0.64 1434055562000000000'
```
 - Wait for 5 mins (or the config time), for NIFI to send the mail ( can be verified on http://localhost:9087/nifi ).
 - Once the mail is sent, check in inbox it must be read, if AWS is setup accordingly.
 - Log into Clari5 Bank website, open Grafana and then open AWS Mail Attachment Metrics Dashboard
 - Check the chart for <tenant_id> bank. You should see one email received.


## Creating / Deploying Collector to each machine.

Following steps are to be run on each machine where EFM/CC is installed

 - Download [mailer-stack-collector.tar.gz](https://cxps-no-reply-mails.s3.amazonaws.com/public-share/mailer-stack-collector.tar.gz)
 - Untar mailer-stack-catcher.tar.gz
 - Run ./install-telegraf.sh on all machines and make sure that no errors occur.
 - Open `remote-runtime/mailer-stack/host-telegraf.conf` and edit the following variables
   - Replace `bank_id` with corresponding `<tenant_id>`
   - Replace `collector_ip` with corresponding IP where mailer-stack is installed
   - Replace `user:password@tcp(host:3306)` with its corresponding values for MYSQL server, if mysql is being used, else comment the whole block starting with [[inputs.mysql]]
   - Replace `Server=host;Port=1433;User Id=user;Password=password;app name=mssql;log=1;` with its corresponding values for MSSQL server, if mssql is being used, else comment the whole block starting with [[inputs.sqlserver]] 
   - Replace `interface_here` with specific interface values.
   - Replace `INSTALLION_DEP_BASE` with absolute path of deployment directory of EFM/CC.
 - Start telegraf using systemctl or service command.
   ```
   sudo systemctl start telegraf.service
   ```
   OR
   ```
   sudo service telegraf.service start
   ```

## Testing

 - Log into the machine where mailer-stack is deployed.
 - To verify if all containers are up and running open Portainer in Browser (http://localhost:9020).
 - Open Nifi (http://localhost:9087/nifi) and go to Test Mail Send Process Group, Update the following properties: In SetAttributes, update path attribute to and replace it with apm/telegraf/<TENANT_ID>. Update PutEmail similar to that mentioned above.
 - Run all the processes in the current process group. To check if Email is being sent, read Troubleshooting of this document.
 - Log into Clari5 Bank website, open Grafana and then open AWS Mail Attachment Metrics Dashboard.
 - Check the chart for <tenant_id> bank. You should see one email received.

# Troubleshooting APM

Follow below given steps to troubleshoot the issue related to mail not reaching Cloud. These same steps are to be followed any time mail stop getting delivered to the cloud system.

*   Check if docker is running. 
    *   Run _docker ps_. If it gives a list of containers that are running, then docker is running. 
    *   If not running, bring up docker using systemctl or service as mentioned in deployment steps above. After starting docker wait for one minute and check if portainer, nifi, telegraf is running, by running docker ps. They should start on their own but if they do not start then run _deploy-mailer-stack.sh_ 
*   Check if Portainer is running
    *   Open _http://localhost:9020_, if portainer is accessible then it is running.
    *   Else follow all the steps for deployment mentioned at the start of this document.
*   Check if nifi is running in Portainer. 
    *   Open _http://localhost:9020_ and click on containers on the left side menu. 
    *   There should be a container with name _mailer-stack_nifi_1_ (Use search option if the list is long)
    *   Check the status of this container, it should be running. 
    *   If you do not find this container, follow all the steps for deployment mentioned at the start of this document.
    *   If status is failed, check the logs to know the reason why it failed, then mark the checkbox for the given container and click the restart button on top of screen. 
    *   If state of the container goes back to failed after restart, report the issue. 
    *   If status is stopped, then just restart as mentioned above.
*   Check if telegraf is running in Portainer.
    *   Open _http://localhost:9020 _and click on containers on left side menu_._
    *   There should be three containers with name _mailer-stack_telegraf_1, mailer-stack_telegraf-slow-freq_1, mailer-stack_telegraf-high-freq_1_ (Use search option if the list is long).
    *   If any of them not running, and the status is failed, click on logs button, make sure to know the reason or which container failed, then mark the checkbox for the given container and click the restart button on top of screen.
    *   If the state of the container goes back to failed after restart, report the issue.
    *   If any of their status is stopped, then just restart as mentioned above.
*   Check if nifi is able to send mails
    *   Open **http://localhost:9070** and open **SendMail** process group. Check if there are any errors in **PutMail** or any other process. To check if there is an error for any of the process, a small red box pops up in upper right corner of the process box. If there is a red box, then hover over the red box and a pop up will show the details of errors occurred. (Refer screenshot)
    *   Following are possible errors that might occur.
    *   Email Credentials incorrect. **Solution**: Stop the PutEmail process, and right click, then configure, update the credentials, start the PutEmail process.
    *   Could not establish connection. **Solution: **Stop the PutEmail process, and right click, then configure, correct the host/port details as they are incorrect, and start the process. OR SMTP server is down and mails are not being sent.
*   When mails are not sent, files that were not sent are moved to _telegraf-error-dir _in the installation directory of mailer-stack. After Nifi is working back again, move files from this directory back to _telegraf-dir_.

# Cloud Setup (For Cloud Support Team)

### UAC Changes
 - Creation of tenent in AWS UAC. URL - https://clari5bank.clari5.com/app/
   - Go to UAC logging into SUPERUSER user.
   - Go to Tenant Screen, add tenant, using a name.
 - Make sure that corresponding DN (<tenant_id>bank.clari5.com) is registered.
 - Log into AWS to create nginx config for new tenant.
 - Copy a tenant nginx config at `~/clari5_home/runtime/conf/<existing_tenant>.nginx.template.conf` with a new name as `<tenant_id>.nginx.template.conf` 
 - Replace all in the new config.
   - `set $tenant_id '<existing_tenant>'` with `set $tenant_id '<tenant_id>'`
   - `server_name superset<existing_tenant>$BASE_DN` with `server_name superset<tenant_id>$BASE_DN`
   - `server_name apm<existing_tenant>$BASE_DN;` with `server_name apm<tenant_id>$BASE_DN;`
   - `server_name <existing_tenant>$BASE_DN;` with `server_name <tenant_id>$BASE_DN;`
 - ADD following line in `clari5web.dockerFile`
```dockerfile
COPY <tenant_id>.nginx.template.conf /tmp/
RUN envsubst '$BASE_DN,$BASE_PORT' < /tmp/<tenant_id>.nginx.template.conf > /etc/nginx/conf.d/<tenant_id>.nginx.conf
```
 - Go the `~/clari5_home/runtime/` dir.
 - Run the following command.
```bash
docker-compose -f build.base-compose.yml build
```
 - Run the following command to update the nginx to accept request coming from new tenant
```bash
docker service update --force runtime-stack_clari5web
```
 - Check the UI using `https://<tenant_id>bank.clari5.com/app`.
 - Create a new Role `APM Role` to access Grafana, add READ capability access to `grafana-dashboards` capability, log into AWS using ROLEMAKER.
 - Authorize the role, log into AWS using ROLEAUTHORIZER.
 - Create users named `GRAFANA`, `APM_USER`, and assign them `APM Role`.
 - Authorize the users, log into AWS using USERAUTHORIZER.

### Influx Changes
 - Log into AWS.
 - Create Database in Influx DB in AWS using following curl command.
```bash
curl -i -XPOST http://localhost:8886/query --data-urlencode "q=CREATE DATABASE <tenant_id>"
```
 - Test using following
```bash
curl -i -XPOST 'http://localhost:8886/write?db=<tenant_id>' --data-binary 'cpu_load_short,host=server01,region=us-west value=0.64 1434055562000000000'
```

### Grafana Changes
 - Log into AWS using Superuser of Clari5Bank.
 - Go to APM Dashboards.
 - In Settings right hand panel -> Admin Settings
 - Create new Organization.
 - Switch to that Organization.
 - Create Influx Datasource for influx and set following values.
   - name: `InfluxDB`
   - URL: `http://172.17.0.1:8886`
   - DATABASE: <tenant_id> 
   - Leave username, password and other fields as default or blank.
 - Left side panel Plus Sign -> Import -> Paste json and paste the json from `./runtime/extra/grafana_dashboards`.
 - Log into all the users for given tenant, and make sure to change the organization for each user in Grafana.
 - Open Grafana using newly created tenant and log into SUPERUSER3, SUPERUSER, GRAFANA, APM_USER.
 - Open Grafana using Clari5 tenant and log into SUPERUSER, navigate to Server Admin setting and now these users would already created. 
 - Give Read access in Grafana to <tenant_id>-GRAFANA, <tenant_id>-APM_USER in above created org, Editor access to <tenant_id>-SUPERUSER3, Admin access to <tenant_id>-SUPERUSER3.

### Nifi Changes
 - Log into AWS using Superuser of Clari5Bank.
 - Open Data Integration in menu.
 - Go to `SaveToInflux` process group.
 - Copy a processor with name `Get <old_tenant_id> out files`, paste it, and edit newly created properties with name `Get <tenant_id> out files` 

### Adding Alerting for new tenant
 - Log into AWS using Superuser of Clari5Bank and open APM Dashboards
 - Go to `AWS Mail Attachment Metrics dashboard`, duplicate any one of the existing chart. There is option for it using More..., when you click on existing chart.
 - Change the name of the chart and where clause in Query to draw Karnataka charts.
 - Alerting is not duplicated by default. Open existing tenant chart in one tab and new tenant chart in new tab. Copy the alerting config manually. Add required channels to it


