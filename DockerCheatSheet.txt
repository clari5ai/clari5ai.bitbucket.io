﻿Doc will be updated at
https://docs.google.com/document/d/1iE9ahgpTw12PdCe8JvgaqdUBZHI-wWA0dDa_cjVPKLg/edit?usp=sharing


Docker Cheat Sheet
Part 1:


## List Docker CLI commands
sudo docker
sudo docker container --help

## Display Docker version and info
sudo docker --version
sudo docker version
sudo docker info

## Execute Docker image
sudo docker run hello-world

## List Docker images
sudo docker image ls

## List Docker containers (running, all, all in quiet mode)
sudo docker container ls
sudo docker container ls --all
sudo docker container ls -aq
	

Part 2:
DockerFile sample:
# Use an official Python runtime as a parent image
FROM python:2.7-slim
# Set the working directory to /app
WORKDIR /app
# Copy the current directory contents into the container at /app
ADD . /app
# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt
# Make port 80 available to the world outside this container
EXPOSE 80
# Make port 7000 available for it be able to access REDIS 
EXPOSE 7000
# Define environment variable
ENV NAME World
# Run app.py when the container launches
CMD ["python", "app.py"]
	

CMD:
sudo docker build -t friendlyhello .
## Create image using this directory's Dockerfile
sudo docker run -p 4000:80 friendlyhello  
## (-d for daemon) Run "friendlyname" mapping port 4000 to 80
sudo docker container stop <hash>           
## Gracefully stop the specified container
sudo docker container kill <hash>         
## Force shutdown of the specified container
sudo docker container rm <hash>        
## Remove specified container from this machine
sudo docker image rm <image id>            
## Remove specified image from this machine
sudo docker tag <image> username/repository:tag  
## Tag <image> for upload to registry
sudo docker push username/repository:tag            
## Upload tagged image to registry
sudo docker run username/repository:tag                   
## Run image from a registry

	Part 3:
docker-compose.yml
version: "3"
services:
 web:
   # replace username/repo:tag with your name and image details
   image: redit/tuts:part2
   deploy:
     replicas: 6
     resources:
       limits:
         cpus: "0.1"
         memory: 50M
     restart_policy:
       condition: on-failure
   ports:
      - "8234:80"
   networks:
      - webnet
networks:
 webnet:

	

CMD:
sudo docker stack ls                                            
## List stacks or apps
sudo docker stack deploy -c <composefile> <appname>  
## Run the specified Compose file
sudo docker service ls                 
## List running services associated with an app
sudo docker service ps <service>       
## List tasks associated with an app
sudo docker inspect <task or container> 
## Inspect task or container
sudo docker container ls -q                                      
## List container IDs
sudo docker stack rm <appname>                             
## Tear down an application
sudo docker swarm leave --force      
## Take down a single node swarm from the manager
	

Custom Registry: link
## Start a new registry as a container
sudo docker run -d \
 -p 9898:5000 \
 --restart=always \
 --name registry \
 -v /mnt/registry:/var/lib/registry \
 registry:2

## to pull an image from all registries (first time will be pulled from docker-hub)
sudo docker pull <image_name> 
## rename the image, to push it with new name
sudo docker tag <image_name> localhost:9898/newimg 
## push new image name
sudo docker push localhost:9898/newimg 

## remove both images
sudo docker image remove localhost:9898/my-ubuntu
sudo docker image remove <image_name>
	



For dev in clari5-ai you would most likely be required to remove sudo


To remove sudo, following steps are required:
## check if docker is not accessible without sudo 
docker container ls  ## this should throw error
sudo groupadd docker
sudo gpasswd -a $USER docker

## LOGOUT current user and login again OR
newgrp docker ## this will only work in current session of shell
docker container ls


## Cleanup for Docker
https://stackoverflow.com/a/41021336

